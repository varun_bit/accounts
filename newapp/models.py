from django.db import models

class Client(models.Model):
	client_name=models.CharField(max_length=50)
	def __str__(self):
		return self.client_name


class Transaction(models.Model):
	trans_client=models.ForeignKey(Client,on_delete=models.CASCADE)
	paticular=models.CharField(max_length=200)
	bill=models.IntegerField(default=0)
	payment=models.IntegerField(default=0)
	billno=models.IntegerField(default=1)
	date = models.DateTimeField(null=True)



# Create your models here.
